import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Category } from 'src/app/model/category';
import { Lead } from 'src/app/model/lead';
import { Parametrs } from 'src/app/model/params';
import { Status } from 'src/app/model/status';
import { ApiServicesService } from 'src/app/services/api-services.service';
import { DirectionService } from 'src/app/services/direction.service';

@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.scss'],
})
export class LeadComponent implements OnInit {
  constructor(private api: ApiServicesService,private formBuilder: FormBuilder,public menu:MenuService,private activeteRoute:ActivatedRoute  ,private message: MessageService, private router: Router,private path: DirectionService) {
    this.getId = this.activeteRoute.snapshot.params['id']
    this.leadForm = this.formBuilder.group({
      link: ['', Validators.required],
      is_express_delivery: false,
      comment : [''],
      category_id: [,Validators.required],
      status_id: [,Validators.required],
    });
   }
  ngOnInit() {
    this.getStatuses();
    this.getCategories();
    this.storeValueForm();
  }
  
  getId?: number | null
  leadForm!: FormGroup;
  statuses  : Status[] = [];
  categories: Category[] = [];


  access: string = ''
  error: string = ''

  storeValueForm() {
    if(this.getId) {
      this.api.getLeadDetails(this.getId).subscribe((data)=> {
        this.leadForm.patchValue(data)
      });
    }
  }

  updateValueForm() {
    if (this.getId) {
      let data = this.leadForm.value;
      this.api.updLead(this.getId, data).subscribe(
        (data: any) => {
          this.access = 'Данные изменены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
          setTimeout(() => {
            this.router.navigate(['/home']);
          }, 2000); 
        },
        (error: any) => {
          console.log('Failed to edit svz document:', error)
        }
      );
    }
  }

  getCategories() {
    this.api.getCategories().subscribe((data)=> {
      this.categories = data;
    });
  }
  getStatuses() {
    this.api.getStatuses().subscribe((data)=> {
      this.statuses = data;
    });
  }
  createLead() {
    const data = this.leadForm.value;
    this.api.addLead(data).subscribe(
      (response: Lead[]) => {
        // this.show.showBlock[0] = false;
        this.access = 'Данные добавлены успешно.'
        this.message.openMessageBar(this.access, 'x', 'green-bar')
        setTimeout(() => {
          this.router.navigate(['/home']);
        }, 2000);
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.error = error
        this.message.openMessageBar(this.error, 'x', 'red-bar')
      }
    );
  }
}
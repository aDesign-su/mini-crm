import { HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, map, throwError } from 'rxjs';
import { User } from '../model/user';
import { environment } from 'src/environments/environment';
import { ResponseHttp } from '../model/responseHttp';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private http: HttpClient) { 
    const storedUser = localStorage.getItem('currentUser');
    this.currentUserSubject = new BehaviorSubject<User | null>(storedUser ? JSON.parse(storedUser) : null);
    this.currentUser = this.currentUserSubject.asObservable();
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.token = localStorage.getItem('currentUser');

    if (this.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.token}`
        }
      });
    }

    return next.handle(request);
  }
  token: any
  getUserToken: any
  info: any
  
  public currentUserSubject: BehaviorSubject<User | null>;
  public currentUser: Observable<User | null>;

  public get currentUserValue(): User | null {
    return this.currentUserSubject.value;
  }
  login(email: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/auth/login`, { email, password })
      .pipe(map(user => {
        // Store the JWT token and user details in local storage
        localStorage.setItem('currentUser', JSON.stringify(user));
        localStorage.setItem('userToken', user.access_token);  // Сохраняем токен
        this.getUserToken = user.access_token;
        
        this.currentUserSubject.next(user);
        return user; 
    }));
  }
  addUser(data: User): Observable<User[]> {
    const url = `${environment.apiUrl}/auth/register`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    );
  }

  getUser() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.getUserToken}`
    });
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/auth/user`,  { headers }).pipe(
      map((data) => {
        // console.log(user.access_token)
        return data;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  logout() {
  // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userRole');
    localStorage.removeItem('userName');
    localStorage.removeItem('userToken');
    localStorage.removeItem('currentUser');

    // Обновление текущего пользователя в BehaviorSubject
    this.currentUserSubject.next(null);
  }
  
  isUserRole1() {
    return this.getRoleFromLocalStorage() === '3';
  }
  getRoleFromLocalStorage(): any {
    return localStorage.getItem('userRole');
  }
  isName() {
    return this.getNameLocalStorage();
  }
  getNameLocalStorage(): any {
    return localStorage.getItem('userName');
  }
}

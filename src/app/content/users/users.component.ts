import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDrawer } from '@angular/material/sidenav';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { RoleList, UserList } from 'src/app/model/userList';
import { ApiServicesService } from 'src/app/services/api-services.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  constructor(private api: ApiServicesService,public menu:MenuService, private formBuilder: FormBuilder,private message: MessageService, public authenticationService: AuthenticationService) {
    this.userForm = this.formBuilder.group({
      name: [''],
      email: [''],
      role_id : [],
    });
   }

  ngOnInit() {
    this.getData();
    this.getStatus();
  }
  @ViewChild('drawer') drawer!: MatDrawer;
  
  displayedColumns: string[] = ['position', 'name', 'email', 'status', 'action'];
  dataSource: UserList[] = [];
  roles  : RoleList[] = [];
  userForm: FormGroup
  showFiller = false;
  getValue!: boolean
  item!: number
  user: any

  section: string = ''
  access: string = ''
  error: string = ''

  getData() {
    this.api.getUserList().subscribe((data)=> {
      this.dataSource = data;
    });
  }
  getStatus() {
    this.api.getStatusList().subscribe((data)=> {
      this.roles = data;
      // console.log(data)
    });
  }
  setValue(element: UserList) {
    this.item = element.id
    this.userForm.patchValue(element)
    this.section = 'level-1'
    this.getValue = true
  }
  saveValue() {
    if (this.item) {
      let saveData = this.userForm.value;
      this.api.updUserList(this.item, saveData).subscribe((data: any) => {
        this.getData()
        this.access = 'Данные обновлены успешно.'
        this.message.openMessageBar(this.access, 'x', 'green-bar')
        this.drawer.close()
      },
      (error: any) => {
        console.log('Failed to edit svz document:', error)
        this.error = 'Не удалось обновить данные.'
        this.message.openMessageBar(this.error, 'x', 'red-bar')
      })
    }
  }
  
  showDetails() {
    this.section = 'level-2'
  }
  delValue(element: any) {
    this.user = element.id
    if(this.getValue == true){
      this.api.delUserList(this.item).subscribe(
        (response: any[]) => {
          this.getData()
          this.access = 'Данные удалены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
          this.drawer.close()
        },
      );
    }
    if (this.user) {
      this.api.delUserList(this.user).subscribe(
        (response: any[]) => {
          this.getData()
          this.access = 'Данные удалены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
          this.drawer.close()
          // console.log(response)
        },
      );
    }
  }
}
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

constructor() { }

isCollapsed = false;
isCollapsedAcr = false;
isFull = false;

toggleCollapse(saveState = true,isDropUp = true,saveBtn = true, saveMenu = true, saveButton = true): void {
  this.isCollapsed = !this.isCollapsed;
}
setAccradion() {
  this.isCollapsedAcr = !this.isCollapsedAcr;
}
setWidthMin () {
  this.toggleCollapse()
}

}

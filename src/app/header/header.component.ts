import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../model/user';
import { ThemeService } from '../helpers/theme.service';
import { ApiServicesService } from '../services/api-services.service';
import { jwtDecode } from "jwt-decode";
import { MenuService } from '../helpers/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(private router: Router,public menu:MenuService,public authenticationService: AuthenticationService,private onDark:ThemeService,private api: ApiServicesService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
   }

  // darkTheme:any = true;
  // status = 'Enable'; 
  
  ngOnInit() {
    // this.darkTheme = localStorage.getItem('theme');
    // this.onDark.theme = localStorage.getItem('dark')

    let token = this.getTokenFromLocalStorage()
    // const token_parts = token.split(/\./);
    // const token_decoded = JSON.parse(window.atob(token_parts[0]));
    // this.currentUser = token_decoded.name;
    var decoded = jwtDecode(token); 
    // console.log(decoded)
    
  }
  currentUser!: User | null;;


  getTokenFromLocalStorage(): any {
    return localStorage.getItem('access');
  }

  navbar: boolean = false
  toggle() {
    this.navbar = !this.navbar
  }




}

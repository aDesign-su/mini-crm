import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { LeadComponent } from './content/lead/lead.component';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { CategoryComponent } from './content/source/category/category.component';
import { StatusComponent } from './content/source/status/status.component';
import { DetailComponent } from './home/detail/detail.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { LoginComponent } from './login/login/login.component';
import { ErrorInterceptor } from './login/login/helpers/error.interceptor';
import { JwtInterceptor } from './login/login/helpers/jwt.interceptor';
import { AuthGuard } from './login/login/helpers/auth.guard';
import { UsersComponent } from './content/users/users.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MessageService } from './helpers/message.service';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { RegisterComponent } from './login/register/register.component';
import { ThemeService } from './helpers/theme.service';
import { MatChipsModule } from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ToolsComponent } from './header/tools/tools.component';
import { MatListModule } from '@angular/material/list'; 
import { TableParametrComponent } from './content/source/category/table-parametr/table-parametr.component';
import { SocialAuthServiceConfig, SocialLoginModule } from '@abacritt/angularx-social-login';
import { GoogleLoginProvider } from '@abacritt/angularx-social-login';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'lead', component: LeadComponent, canActivate: [AuthGuard] },
  { path: 'lead/:id', component: LeadComponent, canActivate: [AuthGuard] },
  { path: 'category', component: CategoryComponent, canActivate: [AuthGuard] },
  { path: 'category/:id', component: TableParametrComponent, canActivate: [AuthGuard] },
  { path: 'status', component: StatusComponent, canActivate: [AuthGuard] },
  { path: 'details', component: DetailComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({ 
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        HeaderComponent,
        ToolsComponent,
        LeadComponent,
        CategoryComponent,
        StatusComponent,
        DetailComponent,
        UsersComponent,
        RegisterComponent,
        TableParametrComponent
    ],
    bootstrap: [AppComponent],
    imports: [
        SocialLoginModule,
        RouterModule.forRoot(appRoutes),
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        NgbModule,
        ReactiveFormsModule,
        MatCardModule,
        MatDividerModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,
        MatMenuModule,
        MatExpansionModule,
        MatTableModule,
        MatIconModule,
        MatSidenavModule,
        MatSnackBarModule,
        MatPaginatorModule,
        MatSlideToggleModule,
        MatChipsModule,
        MatTooltipModule,
        MatSidenavModule,
        MatListModule
      ],
      providers: [
        {
          provide: 'SocialAuthServiceConfig',
          useValue: {
            autoLogin: false,
            providers: [
              {
                id: GoogleLoginProvider.PROVIDER_ID,
                provider: new GoogleLoginProvider('191841067694-fcqgtjp3jha7odid3s5q146mqk7f3a9e.apps.googleusercontent.com')
              }
            ]
          } as SocialAuthServiceConfig,
        },
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        MessageService,
        ThemeService
        // provider used to create fake backend
        // fakeBackendProvider
        ,
        provideHttpClient(withInterceptorsFromDi())
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
   })
export class AppModule { }

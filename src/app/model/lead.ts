import { Category } from "./category";
import { Status } from "./status";

export interface Lead {
    id: number;
    link: string;
    is_express_delivery: boolean;
    comment : string;
    category_id: number;
    status_id: number;
    param_id: number;
    category?: Category;
    status?: Status;
}

export interface Details {
    link: string;
    comment: string;
    category_id: number;
    param_id: number;
    status_id: number;
    category: {
        id: number;
        name: string;
        code: string;
    }
    status: {
        title: string;
        alias: string;
    },
}

export interface Param {
    id: number;
    title: string;
    description: string;
    code_product: string;
    code_manufacturer: string;
    price: number;
}
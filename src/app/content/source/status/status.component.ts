import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatDrawer } from '@angular/material/sidenav';
import { MatTableDataSource } from '@angular/material/table';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Status } from 'src/app/model/status';
import { ApiServicesService } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {
  constructor(private api: ApiServicesService,public menu:MenuService, private formBuilder: FormBuilder,private message: MessageService) { 
    this.statusForm = this.formBuilder.group({
      title: ['', Validators.required],
      alias : ['', Validators.required],
    });
  }
  @ViewChild('drawer') drawer!: MatDrawer;
  statusForm!: FormGroup;
  access: string = ''
  error: string = ''

  ngOnInit() {
    this.getDataSource()
  }

  displayedColumns: string[] = ['number', 'title', 'alias', 'action'];
  dataSource = new MatTableDataSource<Status>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  getDataSource(): void {
    this.api.getStatuses().subscribe(
      (data: any) => {
        this.dataSource.data = data
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  createStatus() {
    const data = this.statusForm.value;
    this.api.addStatuses(data).subscribe(
      (response: Status[]) => {
        this.drawer.close()
        this.getDataSource()
        this.access = 'Данные добавлены успешно.'
        this.message.openMessageBar(this.access, 'x', 'green-bar')
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        this.message.openMessageBar(this.error, 'x', 'red-bar')
      }
    );
  }
  delCategory(element: any) {
    const data = element.id 
    if (data) {
      this.api.delStatuses(data).subscribe(
        (response: Status[]) => {
          this.getDataSource()
          this.access = 'Данные удалены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
        },
      );
    }
  }
}
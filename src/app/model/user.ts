export interface User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    role_id: number;
    token?: string;
    email: string;
}

export interface UserList {
    id: number;
    name: string;
    email: string;
    role_id: number;
    role: {
        status: string;
    }
}
export interface RoleList {
    id: number;
    role: string;
    status: number
}
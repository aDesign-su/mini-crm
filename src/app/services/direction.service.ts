import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DirectionService {

constructor() { 
  this.loadFromLocalStorage(this.setKey, this.setSource);
}
  private setSource = new BehaviorSubject<number | null>(null);
  currentSetKey = this.setSource.asObservable();
  private setKey = 'setKey';

  private loadFromLocalStorage(key: string, source: BehaviorSubject<number | null>): void {
    const value = localStorage.getItem(key);
    const parsedValue = value ? +value : null;
    source.next(parsedValue);
  }
  private saveToLocalStorage(key: string, value: number): void {
    localStorage.setItem(key, value.toString());
  }
  setSetKey(id: number) {
    this.saveToLocalStorage(this.setKey, id);
    this.setSource.next(id);
  }
  clearSetKey() {
    localStorage.removeItem(this.setKey);
    this.setSource.next(null);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SocialAuthService } from '@abacritt/angularx-social-login';

@Injectable({
  providedIn: "root",
})
export class Oauth2Service {
  constructor(private http: HttpClient, private socialAuthService: SocialAuthService) {}

  private apiUrl = 'http://localhost:8000/api/auth/google';

  sendGoogleToken(idToken: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.apiUrl, { idToken }, { headers });
  }
}

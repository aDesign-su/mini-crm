import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ThemeService } from 'src/app/helpers/theme.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {

  constructor(private router: Router, public authenticationService: AuthenticationService,private onDark:ThemeService) { }

  darkTheme:any = true;
  status = 'Enable'; 
  
  ngOnInit() {
    this.darkTheme = localStorage.getItem('theme');
    this.onDark.theme = localStorage.getItem('dark')
  }
  get dark() {
    return this.onDark.theme === 'dark';
  }
  data() {
    if (this.darkTheme = true) {
      this.darkTheme = localStorage.setItem('theme', this.darkTheme);
    }
    this.darkTheme = !this.darkTheme;
    this.status = this.darkTheme ? 'Enable' : 'Disable';
  }
  test() {
   this.darkTheme = localStorage.removeItem('theme');
  }
  set dark(enabled: boolean) {
    
    if (this.onDark.theme = 'dark') {
      this.onDark.theme = enabled ? 'dark' : null;
      this.onDark.theme = localStorage.setItem('dark', this.onDark.theme);
    }
    if (this.onDark.theme = null) {
      this.onDark.theme = enabled ? 'dark' : null;
      this.onDark.theme = localStorage.removeItem('dark');
    }

    this.onDark.theme = enabled ? 'dark' : null;
    console.log(this.onDark.theme)
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/']);
  }

  onFileSelected(event: any): void {
    const file: File = event.target.files[0];

    // if (file) {
    //   const formData = new FormData();
    //   formData.append('file', file, file.name);
    //   this.params.uploadRegisterEstimatesGSFX(formData).subscribe(
    //     (data) => {
    //       this.theme.openSnackBar('Смета загружена');
    //       // this.getDataRegisterEstimatesGSFX()

    //     },
    //     (error: any) => {
    //       console.error('Error fetching data:', error);
    //     }
    //   );
    // }
  }
  mobileMenu!: boolean
  isOpen() {
    this.mobileMenu = !this.mobileMenu
  }
}

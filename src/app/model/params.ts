export interface Parametrs {
    id: number,
    title: string,
    description: string,
    code_product: string,
    code_manufacturer: number,
    price: number,
    category_id: number,
    category: {
        id: number,
        name: string,
        code: string,
    }
}

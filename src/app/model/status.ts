export interface Status {
    id: number;
    title: string;
    alias: string;
}
import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { Lead } from '../model/lead';
import { Router } from '@angular/router';
import { DirectionService } from '../services/direction.service';
import { MessageService } from '../helpers/message.service';
import { AuthenticationService } from '../services/authentication.service';
import { MenuService } from '../helpers/menu.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private api: ApiServicesService,public menu:MenuService, private path: DirectionService,private router: Router,private message: MessageService,public authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.getData()
  }

  filteredLeads: Lead[] = [];
  leads: Lead[] = [];
  setId?: number;
  access: string = ''
  error: string = ''
  

  getData() {
    this.api.getLeads().subscribe((data)=> {
      this.leads = data;
      this.applyFilter();
    });
  }

  addParam(id: number) {
    this.setId = id 
    if(id) { 
      this.router.navigate(['/param', id]); 
      this.path.setSetKey(this.setId);
    } 
  }

  onEdit(id: number) {
    this.setId = id 
    if(id) { 
      this.router.navigate(['/lead', id]); 
      this.path.setSetKey(this.setId);
    } 
  }

  onSubmit(id: number) {
    this.setId = id 
    if(id) { 
      this.router.navigate(['details']); 
      this.path.setSetKey(this.setId);
    } 
  }
  delLeadList(element: any) {
    const lead = element.id
    if (lead) {
      this.api.delLead(lead).subscribe(
        (response: any[]) => {
          this.getData()
          this.access = 'Данные удалены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
        },
      );
    }
  }
  filterOptions: any = {
    новый: false,
    обработка: false,
    завершен: false,
  };
  toggleFilter(option: string) {
    this.filterOptions[option] = !this.filterOptions[option];
    this.applyFilter();
  }
  applyFilter() {
    this.filteredLeads = this.leads.filter((lead: any) => {
      if (
        (!this.filterOptions['новый'] || lead.status?.title === 'Новый') &&
        (!this.filterOptions['обработка'] || lead.status?.title === 'Обработка') &&
        (!this.filterOptions['завершён'] || lead.status?.title === 'Завершён')
      ) {
        return true;
      }
      return false;
    });
  }
}

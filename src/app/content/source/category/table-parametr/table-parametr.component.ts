import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatDrawer } from '@angular/material/sidenav';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Parametrs } from 'src/app/model/params';
import { ApiServicesService } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-table-parametr',
  templateUrl: './table-parametr.component.html',
  styleUrls: ['./table-parametr.component.css']
})
export class TableParametrComponent implements OnInit {

  constructor(public menu:MenuService,private activeteRoute:ActivatedRoute,private api: ApiServicesService,private formBuilder: FormBuilder,private message: MessageService) { 
    this.getId = this.activeteRoute.snapshot.params['id']
    this.parametrForm = this.formBuilder.group({
      title: ['', Validators.required],
      description : ['', Validators.required],
      code_product : [''],
      code_manufacturer : [''],
      price : [0],
      category_id : [this.getId],
    });
  }

  parametrForm!: FormGroup;
  getId: number

  access: string = ''
  error: string = ''

  ngOnInit() {
    this.getDataSource()
  }

  displayedColumns: string[] = ['number', 'title', 'description', 'code_product', 'code_manufacturer', 'action'];
  dataSource = new MatTableDataSource<Parametrs>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild('drawer') drawer!: MatDrawer;
  
  getDataSource(): void {
    this.api.getParamsSource().subscribe(
      (data: Parametrs[]) => {
        this.dataSource.data =  data.filter(item => item.category_id == this.getId);
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  createParametr() {
    const data = this.parametrForm.value;
    this.api.addParamsSource(data).subscribe(
      (response: Parametrs[]) => {
        this.drawer.close()
        this.getDataSource()
        this.access = 'Данные добавлены успешно.'
        this.message.openMessageBar(this.access, 'x', 'green-bar')
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.error = error
        this.message.openMessageBar(this.error, 'x', 'red-bar')
      }
    );
  }
  delParametr(element: any) {
    const data = element.id
    if (data) {
      this.api.delParamsSource(data).subscribe(
        (response: Parametrs[]) => {
          this.getDataSource()
          this.access = 'Данные удалены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
        },
      );
    }
  }
}

/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TableParametrComponent } from './table-parametr.component';

describe('TableParametrComponent', () => {
  let component: TableParametrComponent;
  let fixture: ComponentFixture<TableParametrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableParametrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableParametrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatDrawer } from '@angular/material/sidenav';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Category } from 'src/app/model/category';
import { UserList } from 'src/app/model/userList';
import { ApiServicesService } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  constructor(private api: ApiServicesService,public menu:MenuService,private formBuilder: FormBuilder,private message: MessageService,public router: Router) { 
    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required],
      code : ['', Validators.required],
      description : [0],
      img : [0],
    });
  }
  @ViewChild('drawer') drawer!: MatDrawer;
  
  categoryForm!: FormGroup;
  access: string = ''
  error: string = ''

  ngOnInit() {
    this.getDataSource()
  }

  displayedColumns: string[] = ['number', 'name', 'code', 'action'];
  dataSource = new MatTableDataSource<Category>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  getDataSource(): void {
    this.api.getCategories().subscribe(
      (data: any) => {
        this.dataSource.data = data
        console.log(data)
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  createCategory() {
    const data = this.categoryForm.value;
    this.api.addCategories(data).subscribe(
      (response: Category[]) => {
        this.drawer.close()
        this.getDataSource()
        this.access = 'Данные добавлены успешно.'
        this.message.openMessageBar(this.access, 'x', 'green-bar')
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.error = 'Не удалось добавить данные. Ошибка #400 Bad Request.'
        this.message.openMessageBar(this.error, 'x', 'red-bar')
      }
    );
  }
  delCategory(element: any) {
    const data = element.id
    if (data) {
      this.api.delCategories(data).subscribe(
        (response: Category[]) => {
          this.getDataSource()
          this.access = 'Данные удалены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
        },
      );
    }
  }
  redirect(id: number) {
    console.log(id)
    if(id) {
      this.router.navigate(['/category', id]);
    }
  }
}
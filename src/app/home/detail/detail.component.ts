import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Details, Lead, Param } from 'src/app/model/lead';
import { Parametrs } from 'src/app/model/params';
import { ApiServicesService } from 'src/app/services/api-services.service';
import { DirectionService } from 'src/app/services/direction.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  constructor(private api: ApiServicesService,private path: DirectionService,private message: MessageService,public menu:MenuService,private router: Router,private formBuilder: FormBuilder) { 
    this.path.currentSetKey.subscribe((id) => {
      this.getId = id
      console.log(this.getId)
    });
    this.paramForm = this.formBuilder.group({
      param_id: [,Validators.required],
      lead_id: [this.getId],
    });
  }
  ngOnInit() {
    this.getData()
    this.getDataDetails()
    this.getParams()
    this.getDataInfo()
    this.sourceData()
  }

  paramForm!: FormGroup;
  paramId!: number
  filter: Parametrs[] = []
  info: Param[] = []
  data: any[] = []
  params: Parametrs[] = [];
  getId?: number | null
  dataSource: Details | null = null;
  array: any[] = []

  access: string = ''
  error: string = ''

  getDataInfo() {
    this.api.getSourceInfo().subscribe((data)=> {
      this.data = data
    });
  }
  getDataDetails() {
    this.api.getParamDetails(this.getId).subscribe((data: Param[])=> {
      this.info = data
    });
  }
  getData() {
    this.api.getLeadDetails(this.getId).subscribe((data)=> {
      this.dataSource = data
      this.paramId = this.dataSource.category.id
    });
  }
  getParams() {
    this.api.getParamsSource().subscribe((data)=> {
      this.params = data;
      setTimeout(() => {
        this.filter = data.filter(item => item.category_id == this.paramId);
      }, 2000);
    });
  }
  sourceData() {
    forkJoin([
      this.api.getSourceInfo(),
      this.api.getParamDetails(this.getId)
    ]).subscribe(([dataInfo, dataDetails]: [any, Param[]]) => {
      const mergedArray = [...dataInfo, ...dataDetails];
      let title = dataDetails.map(item => ({
        title: item.title,
        description: item.description
      }));
      // Теперь фильтруем массив, чтобы получить только элементы, где id == param_id
      this.array = mergedArray.filter(item => {
        if (item.lead_id == this.getId) { // Проверяем совпадение lead_id
          // Проверяем совпадение id и param_id
          return dataDetails.some(detail => detail.id == item.param_id);
        }
        return false;
      }).map(item => ({
        id: item.id,
        lead_id: item.lead_id,
        param_id: item.param_id,
        title: item.title || item.category?.title,
        description: item.description || item.category?.description
      }));
      // Добавляем title в итоговый массив
      this.array.forEach((item, index) => {
        item.title = title[index].title;
        item.description = title[index].description;
      });
    });
  }
  createLeadParam() {
    const data = this.paramForm.value;
    this.api.addLeadParams(data).subscribe(
      (response: Parametrs[]) => {
        this.access = 'Данные добавлены успешно.'
        this.message.openMessageBar(this.access, 'x', 'green-bar')
        this.sourceData()
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.error = error
        this.message.openMessageBar(this.error, 'x', 'red-bar')
      }
    );
  }
  delLeadParam(element: any) {
    const info = element
    if (info) {
      this.api.delParamDetails(info).subscribe(
        (response: any[]) => {
          this.sourceData()
          this.access = 'Данные удалены успешно.'
          this.message.openMessageBar(this.access, 'x', 'green-bar')
        },
      );
    }
  }
}

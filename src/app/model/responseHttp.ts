export interface ResponseHttp {
    status: boolean;
    errors: {

    };
    data : {
        user: any[],
        param: any[],
        items: any[],
        info: any[],
        item: any
    }
}
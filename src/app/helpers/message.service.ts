import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class MessageService {
constructor(private _snackBar: MatSnackBar) { }

  openMessageBar(message: string, action: string, className: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

}

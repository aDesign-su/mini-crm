import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Oauth2Service } from 'src/app/services/oauth2.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private formBuilder: FormBuilder,private route: ActivatedRoute,private router: Router,private authenticationService: AuthenticationService,private authService: SocialAuthService,private authBackend: Oauth2Service) {
  // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) { 
      this.router.navigate(['/home']);
    }
   }
   
   user: SocialUser | undefined;
   loggedIn: boolean | undefined;

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log('Google user:', user);
      if (user) {
        // Отправляем idToken на сервер Laravel
        this.authBackend.sendGoogleToken(user.idToken).subscribe(
          (response: any) => {
            console.log('Successfully authenticated with Laravel:', response);
            // Сохраняем пользователя и токен в localStorage
            localStorage.setItem('currentUser', JSON.stringify(response.user));
            localStorage.setItem('userToken', response.token);
        
            // Обновляем currentUserSubject
            this.authenticationService.currentUserSubject.next(response.user);
        
            // Выполняем редирект
            this.router.navigate(['/home']);
          },
          (error:any) => {
            console.error('Authentication failed:', error);
          }
        );
      }
    });
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    this.initializeGoogleButton();
  }

  loginForm!: FormGroup;
  loading = false;
  submitted = false;
  returnUrl!: string;
  error = '';

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.authenticationService.login(this.f['email'].value, this.f['password'].value)
    .pipe(first())
    .subscribe(data => {
      // Выполнять GET-запрос здесь, когда токен получен
      this.authenticationService.getUser().subscribe(
        (userData: any) => {
          this.authenticationService.info = userData.role_id
          localStorage.setItem('userRole', JSON.stringify(userData.role_id));
          localStorage.setItem('userName', userData.name);
          console.log('User Data:', userData);
        },
        (error: any) => {
          console.error('Error fetching user data:', error);
        }
      );
      
      this.router.navigate([this.returnUrl]);
    }, error => {
      this.error = error;
      this.loading = false;
    });
  }
  initializeGoogleButton(): void {
    google.accounts.id.initialize({
      client_id: '191841067694-fcqgtjp3jha7odid3s5q146mqk7f3a9e.apps.googleusercontent.com', // Укажите ваш client_id
      callback: this.handleCredentialResponse.bind(this)
    });

    google.accounts.id.renderButton(
      document.getElementById('google-signin-button'),
      { theme: 'outline', size: 'large' }
    );
  }
  handleCredentialResponse(response: any) {
    console.log('Encoded JWT ID token: ' + response.credential);
  
    // Отправляем idToken на сервер Laravel
    this.authBackend.sendGoogleToken(response.credential).subscribe(
      (res: any) => {
        console.log('Successfully authenticated with Laravel:', res);
        // Сохраняем пользователя и токен в localStorage
        localStorage.setItem('currentUser', JSON.stringify(res.user));
        localStorage.setItem('userToken', res.token);
  
        // Обновляем currentUserSubject
        this.authenticationService.currentUserSubject.next(res.user);
  
        // Редирект на домашнюю страницу
        this.router.navigate(['/home']);
      },
      (error: any) => {
        console.error('Authentication failed:', error);
      }
    );
  } 
  // Метод для повторного рендеринга кнопки при необходимости
  resetGoogleSignIn(): void {
    this.initializeGoogleButton();
  }
}

declare const google: any;
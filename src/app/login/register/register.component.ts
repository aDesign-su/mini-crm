import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/helpers/message.service';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService,private formBuilder: FormBuilder,private message: MessageService,private router: Router) {
    this.userForm = this.formBuilder.group({
      password: ['',Validators.required],
      name: ['',Validators.required],
      email : ['',Validators.required],
      role_id : [1],
    });
   }

  ngOnInit() {
  }

  userForm!: FormGroup;
  access: string = ''
  error: string = ''

  onSubmit() {
    const data = this.userForm.value;
    this.authenticationService.addUser(data).subscribe(
      (response: User[]) => {
        // this.show.showBlock[0] = false;
        this.access = 'Регистрация прошла успешно!'
        this.message.openMessageBar(this.access, 'x', 'green-bar')
        setTimeout(() => {
          this.router.navigate(['/']);
        }, 2000);
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        this.error = error
        this.message.openMessageBar(this.error, 'x', 'red-bar')
      }
    );
  }

}

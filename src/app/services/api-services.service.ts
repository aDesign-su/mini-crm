import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, map, throwError } from 'rxjs';
import { Category } from '../model/category';
import { ResponseHttp } from '../model/responseHttp';
import { Status } from '../model/status';
import { Details, Lead, Param } from '../model/lead';
import { RoleList, UserList } from '../model/userList';
import { Parametrs } from '../model/params';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  constructor(private http: HttpClient) { }
  // baseUrl = 'http://back-minicrm.development-a.tw1.ru/api'

  addLeadParams(data: Parametrs): Observable<Parametrs[]> {
    const url = `${environment.apiUrl}/info`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }

  addLead(data: Lead): Observable<Lead[]> {
    const url = `${environment.apiUrl}/leads`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  updLead(id: number, data: Lead): Observable<Lead[]> {
    const url = `${environment.apiUrl}/leads/${id}/`;
    return this.http.put<ResponseHttp>(url, data).pipe(
      map((data) => {
        return data.data.item;
      }),
      catchError((error) => {
        return throwError(error)
      })
    );
  }
  getParamDetails(id: any) : Observable<Param[]> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/leads/${id}`).pipe(
      map((data) => {
        return data.data.item.param;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  delParamDetails(id: number): Observable<Param[]> {
    const url = `${environment.apiUrl}/info/${id}/`;
    return this.http.delete<Param[]>(url);
  }
  getSourceInfo() : Observable<any[]> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/info`).pipe(
      map((data) => {
        return data.data.info;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  getLeadDetails(id: any) : Observable<Details> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/leads/${id}`).pipe(
      map((data) => {
        return data.data.item;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }

  getLeads() : Observable<Lead[]> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/leads`).pipe(
      map((data) => {
        return data.data.item;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  delLead(id: number): Observable<Lead[]> {
    const url = `${environment.apiUrl}/leads/${id}/`;
    return this.http.delete<Lead[]>(url);
  }
  getParamsSource() : Observable<Parametrs[]> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/params`).pipe(
      map((data) => {
        return data.data.param;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  addParamsSource(data: Parametrs): Observable<Parametrs[]> {
    const url = `${environment.apiUrl}/params`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  delParamsSource(id: number): Observable<Parametrs[]> {
    const url = `${environment.apiUrl}/params/${id}/`;
    return this.http.delete<Parametrs[]>(url);
  }
  getCategories() : Observable<Category[]> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/categories`).pipe(
      map((data) => {
        return data.data.items;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  addCategories(data: Category): Observable<Category[]> {
    const url = `${environment.apiUrl}/categories`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    );
  }
  delCategories(id: number): Observable<Category[]> {
    const url = `${environment.apiUrl}/categories/${id}/`;
    return this.http.delete<Category[]>(url);
  }

  getStatuses() : Observable<Status[]> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/statuses`).pipe(
      map((data) => {
        return data.data.items;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  addStatuses(data: Status): Observable<Status[]> {
    const url = `${environment.apiUrl}/statuses`;
    return this.http.post(url, data).pipe(
      map((response: any) => response)
    );
  }
  delStatuses(id: number): Observable<Status[]> {
    const url = `${environment.apiUrl}/statuses/${id}/`;
    return this.http.delete<Status[]>(url);
  }
  
  getUserList() : Observable<UserList[]> {
    return this.http.get<ResponseHttp>(`${environment.apiUrl}/users`).pipe(
      map((data) => {
        return data.data.user;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
  delUserList(id: number): Observable<UserList[]> {
    const url = `${environment.apiUrl}/users/${id}/`;
    return this.http.delete<UserList[]>(url);
  }
  
  updUserList(id: number, data: UserList): Observable<UserList[]> {
    const url = `${environment.apiUrl}/users/${id}/`;
    return this.http.put<ResponseHttp>(url, data).pipe(
      map((data) => {
        return data.data.user;
      }),
      catchError((error) => {
        return throwError(error)
      })
    );
  }
  getStatusList() : Observable<RoleList[]> {
    return this.http.get<any>(`${environment.apiUrl}/roleList`).pipe(
      map((data) => {
        return data.data;
      }),
      catchError((error) => {
        return throwError(error)
      })
    )
  }
}
